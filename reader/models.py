from django.db import models

# Create your models here.

class Xlsx(models.Model):
    region = models.CharField(max_length=255)
    country = models.CharField(max_length=255)
    item_type = models.CharField(max_length=255)
    sales_channel = models.CharField(max_length=255)
    order_priority = models.CharField(max_length=255)
    order_date = models.DateField()
    order_id = models.IntegerField()
    ship_date = models.DateField()
    units_sold = models.IntegerField()
    unit_price = models.FloatField()
    unit_cost = models.FloatField()
    total_revenue = models.FloatField()
    total_cost = models.FloatField()
    totla_profit = models.FloatField()
